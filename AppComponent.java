/*
 * Copyright 2019-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.mindos.app;

//import org.apache.felix.scr.annotations.Activate;
//import org.apache.felix.scr.annotations.Component;
//import org.apache.felix.scr.annotations.Deactivate;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.apache.felix.scr.annotations.Service;
import org.onosproject.net.Device;
import org.onosproject.net.DeviceId;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.device.PortStatistics;
import org.onosproject.net.packet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.apache.felix.scr.annotations.Reference;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
//import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.onosproject.core.CoreService;
import org.onosproject.core.ApplicationId;


import org.onosproject.net.flow.TrafficSelector;
import org.onosproject.net.flow.DefaultTrafficSelector;

import org.onlab.packet.Ethernet;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.*;


/**
 * Skeletal ONOS application component.
 */
@Component(immediate = true)
public class AppComponent {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected CoreService coreService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected PacketService packetService;

    PacketProcessor pktProcessor = new MinDos();
    private ApplicationId appID;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    protected DeviceService deviceService;

    @Activate
    protected void activate() {
        appID = coreService.registerApplication("org.mindos.app");

        //add a listener for the pkt in event with priority
        packetService.addProcessor(pktProcessor, PacketProcessor.director(1));

        //add matching rules to incoming packespecifiedts
        TrafficSelector.Builder selector = DefaultTrafficSelector.builder();

        //have a look at what type of packets do we want to handle
        //if attackers always use spoofed destination ip address, it will always be an ARP packet
        //as there would not be any switch that knows the destination host
        selector.matchEthType(Ethernet.TYPE_ARP);
        packetService.requestPackets(selector.build(), PacketPriority.REACTIVE, appID, Optional.empty());

        selector.matchEthType(Ethernet.TYPE_IPV4);
        packetService.requestPackets(selector.build(), PacketPriority.REACTIVE, appID, Optional.empty());

        log.info("started");
    }


    private class MinDos implements PacketProcessor {
        //READ MORE ABOUT THREAD CONSUMER AND PRODUCER:
        //https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/BlockingQueue.html
        //thread safe data structures, however data retrieval does not block other operations, meaning that the data
        //retrieved might not be the latest data if there is insert/deletion operation after retrieval
        private final ConcurrentHashMap<DeviceId, LinkedBlockingQueue<InboundPacket>> logicalQueues;
        private final LinkedBlockingQueue<DeviceId> newLogicalQueues;

        //construct a process queue here
        //spin up trust value management service and let it process the process queue to compute trust values
        private final ExecutorService trustValueManagerServiceExecutor;
        //spin up logical queue service and let it pull packets that has been giving a trust value from the queue
        private final ExecutorService logicalQueueServiceExecutor;

        private final ExecutorService priorityQueueManagerExecutor;

        private final ExecutorService timeSliceProcessor;

        MinDos() {
            logicalQueues = new ConcurrentHashMap<>();
            newLogicalQueues = new LinkedBlockingQueue<>();

            //TODO: use newCachedThreadPool if we want unbounded number of threads, otherwise testing is
            // required to adjust it
            trustValueManagerServiceExecutor = Executors.newFixedThreadPool(100,
                    Executors.defaultThreadFactory());
            logicalQueueServiceExecutor = Executors.newFixedThreadPool(100, Executors.defaultThreadFactory());
            priorityQueueManagerExecutor = Executors.newSingleThreadExecutor(Executors.defaultThreadFactory());
            timeSliceProcessor = Executors.newSingleThreadExecutor(Executors.defaultThreadFactory());
        }

        @Override
        public void process(PacketContext pktIn) {
            //add inbound packet to logical queues, may fail due to full queue
            logicalQueueServiceExecutor.submit(new LogicalQueueService(logicalQueues, pktIn.inPacket(), newLogicalQueues));
//            priorityQueueManagerExecutor.submit(new PriorityQueueManager(newLogicalQueues, logicalQueues));
            //add new service to the queue for new logical queue
//            pktIn.treatmentBuilder().setOutput(PortNumber.FLOOD);
            log.info("received packet");
//            while(true) {
//
//            }
//            pktIn.block();
//            pktIn.send();
            // TODO: figure out how to pass specific packets to the next processor (ie: reactive forwarder)
        }
    }

    @Deactivate
    protected void deactivate() {
        log.info("Stopped");
        packetService.removeProcessor(pktProcessor);
        withdrawIntercepts();
    }

    private void withdrawIntercepts() {
        TrafficSelector.Builder selector = DefaultTrafficSelector.builder();
        //cancel the subscription for the ipv4 packets
        selector.matchEthType(Ethernet.TYPE_IPV4);
        packetService.cancelPackets(selector.build(), PacketPriority.REACTIVE, appID);
        selector.matchEthType(Ethernet.TYPE_ARP);
        //cancel the subscription for arp packets
        packetService.cancelPackets(selector.build(), PacketPriority.REACTIVE, appID);
    }

}
