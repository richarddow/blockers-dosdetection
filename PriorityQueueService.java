package org.mindos.app;


import org.onosproject.net.DeviceId;
import org.onosproject.net.Link;
import org.onosproject.net.packet.InboundPacket;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Multithreaded queue that check manages all of the priority queues
 * corresponding to each device
 * It will keep checking if there is any new packets in the logical queues
 * to process them into their respective queues if not full
 *
 */
public class PriorityQueueService extends Thread {

    private final DeviceId deviceId;
    private final LinkedBlockingQueue<InboundPacket> logicalQueue;
    private final ConcurrentHashMap<Integer, LinkedBlockingQueue<InboundPacket>> priorityQueues;

    PriorityQueueService(DeviceId deviceId, LinkedBlockingQueue<InboundPacket> logicalQueue) {
        this.deviceId = deviceId;
        this.logicalQueue = logicalQueue;
        this.priorityQueues = new ConcurrentHashMap<>();
    }

    /**
     * the thread will be running constantly until the thread is shutdown by the executor
     */
    public void run() {
        while(true) {
            try {
                InboundPacket nextPacket = this.logicalQueue.take();
                // TODO: look up the trust value of the packet in the trust value management module
                int trustValue = 0;
                this.insertPriorityQueue(trustValue, nextPacket);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

    private void insertPriorityQueue(int trustValue, InboundPacket inboundPacket) {
        if (priorityQueues.get(trustValue) != null) {
            priorityQueues.get(trustValue).offer(inboundPacket);
        } else {
            LinkedBlockingQueue<InboundPacket> newPriorityQueue = new LinkedBlockingQueue<>();
            newPriorityQueue.offer(inboundPacket);
            priorityQueues.put(trustValue, newPriorityQueue);
        }
    }


}
