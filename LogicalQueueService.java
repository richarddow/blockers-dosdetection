package org.mindos.app;

import org.onosproject.net.DeviceId;
import org.onosproject.net.packet.InboundPacket;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Takes in process queue and split them into logical queue based
 * on the switch that the packet is from
 * required: switch id, inbound packet, logical queue
 * could be multithreaded
 */
public class LogicalQueueService extends Thread {

    private static final ThreadLocal<InboundPacket> inboundPacket = new ThreadLocal<>();
    private final ConcurrentHashMap<DeviceId, LinkedBlockingQueue<InboundPacket>> logicalQueues;
    private final LinkedBlockingQueue<DeviceId> newLogicalQueue;

    LogicalQueueService(ConcurrentHashMap<DeviceId, LinkedBlockingQueue<InboundPacket>> logicalQueues,
                        InboundPacket inboundPacket, LinkedBlockingQueue<DeviceId> newLogicalQueue) {
        LogicalQueueService.inboundPacket.set(inboundPacket);
        this.logicalQueues = logicalQueues;
        this.newLogicalQueue = newLogicalQueue;
    }

    public void run() {
        addInboundPacket(inboundPacket.get());
    }

    /**
     * Add incoming packets to the logical queue associated with its switch id
     * @param inboundPacket the inbound packet at the controller
     * @return true if the logical queue is not full, else false as insertion failed
     */
    private boolean addInboundPacket(InboundPacket inboundPacket) {
        //create a new logical queue if it does not exist
        if (!logicalQueues.containsKey(inboundPacket.receivedFrom().deviceId())) {
            if (this.addQueue(inboundPacket.receivedFrom().deviceId(), inboundPacket)) {

            }
        } else {
            try {
                if (!logicalQueues.get(inboundPacket.receivedFrom().deviceId()).
                        offer(inboundPacket, 5, TimeUnit.SECONDS)) {
                    return false;
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
//        return true;
        return false;
    }

    /**
     * this add function is synchronized to prevent queue duplication, making sure that
     * there would only a queue for each device in the hashmap
     * @param deviceId the id of the switch
     * @param inboundPacket the inbound packet at the controller
     * @return true if a new queue is created, false if otherwise
     */
    synchronized private boolean addQueue(DeviceId deviceId, InboundPacket inboundPacket) {
        if (!logicalQueues.containsKey(deviceId)) {
            //TODO: modify the capacity of the queue during testing
            LinkedBlockingQueue<InboundPacket> newQueue = new LinkedBlockingQueue<>(1000);
            try {
                //TODO: handle the case where the
                if (!newQueue.offer(inboundPacket, 5, TimeUnit.SECONDS)) {
                    return false;
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
            logicalQueues.put(inboundPacket.receivedFrom().deviceId(), newQueue);
            //add the new queue to a queue for priority manager to take it and create a new service for splitting
            //it into priority queues
            newLogicalQueue.offer(inboundPacket.receivedFrom().deviceId());
            return true;
        }
        return false;
    }

    /**
     * @return current logical queues corresponding to each switch
     */
    public ConcurrentHashMap<DeviceId, LinkedBlockingQueue<InboundPacket>> getLogicalQueues() {
        return logicalQueues;
    }

}
