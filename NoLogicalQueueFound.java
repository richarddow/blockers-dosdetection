package org.mindos.app;

public class NoLogicalQueueFound extends Exception {

    NoLogicalQueueFound(String message) {
        super(message);
    }
}
