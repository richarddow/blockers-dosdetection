package org.mindos.app;

import org.onosproject.net.DeviceId;
import org.onosproject.net.packet.InboundPacket;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * single threaded priority queue manager
 * it will take the deviceId from the newLogicalQueue and create a new PriorityQueueService for each devices
 */
public class PriorityQueueManager extends Thread {

    private final LinkedBlockingQueue<DeviceId> newLogicalQueue;
    private final ExecutorService priorityQueueServiceExecutor;
    private final ConcurrentHashMap<DeviceId, LinkedBlockingQueue<InboundPacket>> logicalQueues;

    PriorityQueueManager(LinkedBlockingQueue<DeviceId> newLogicalQueue,
                         ConcurrentHashMap<DeviceId, LinkedBlockingQueue<InboundPacket>> logicalQueues) {
        this.newLogicalQueue = newLogicalQueue;
        // TODO: require adjustments to number of threads in pool, the number here should be equal to the number of
        //  logical queues
        this.priorityQueueServiceExecutor = Executors.newFixedThreadPool(1000, Executors.defaultThreadFactory());
        this.logicalQueues = logicalQueues;
    }

    @Override
    public void run() {
        while (true) {
            try {
                DeviceId newDevice = newLogicalQueue.take();

                if (logicalQueues.get(newDevice) != null) {
                    priorityQueueServiceExecutor.submit(new PriorityQueueService(newDevice,
                            logicalQueues.get(newDevice)));
                } else {
                    throw new NoLogicalQueueFound("No logical found for this deviceId");
                }


            } catch (InterruptedException | NoLogicalQueueFound e) {
                e.printStackTrace();
            }
        }


    }
}
